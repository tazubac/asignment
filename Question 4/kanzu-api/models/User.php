<?php

namespace Kanzu\Models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $fillable = [
        'name',
        'email',
        'role', // Engineer or Project Manager
    ];

    public function projects()
    {
        if ($this->role === 'engineer') {
            return $this->hasMany(Project::class, 'developer_id');
        } else {
            return $this->hasMany(Project::class, 'project_manager_id');
        }
    }
}
