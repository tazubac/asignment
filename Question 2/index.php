<?php
//A. Functions using Regular Expressions (regex)

//1. extract_email(paragraph):
$paragraph = "This is a paragraph and it has to find 256781123456,testemail@gmail.com and https://kanzucode.com/";
function extract_email($paragraph) {
  $email_regex = "/[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*/";
  preg_match($email_regex, $paragraph, $matches);
  if (isset($matches[0])) {
    return $matches[0];
  } else {
    return null;
  }
}

//2. extract_phone_number(paragraph):
function extract_phone_number($paragraph) {
  $phone_regex = "/\d{10,15}/";  # Matches 10 to 15 digits
  preg_match($phone_regex, $paragraph, $matches);
  if (isset($matches[0])) {
    return $matches[0];
  } else {
    return null;
  }
}

//extract_url(paragraph):
function extract_url($paragraph) {
  $url_regex = "/https?://[^\s]+/";  # Matches https or http urls
  preg_match($url_regex, $paragraph, $matches);
  if (isset($matches[0])) {
    return $matches[0];
  } else {
    return null;
  }
}


//B. Functions without Regular Expressions (regex)

function extract_email_no_regex($paragraph) {
  $parts = explode(" ", $paragraph);
  foreach ($parts as $part) {
    if (strpos($part, "@") !== false && strpos($part, ".") !== false) {
      return $part;
    }
  }
  return null;
}

//extract_phone_number_no_regex(paragraph):
function extract_phone_number_no_regex($paragraph) {
  $digits = "";
  for ($i = 0; $i < strlen($paragraph); $i++) {
    if (is_numeric($paragraph[$i])) {
      $digits .= $paragraph[$i];
    }
  }
  if (strlen($digits) >= 10 && strlen($digits) <= 15) {  // Adjust range for expected phone number length
    return $digits;
  } else {
    return null;
  }
}
//extract_url_no_regex(paragraph):
function extract_url_no_regex($paragraph) {
  $parts = explode(" ", $paragraph);
  foreach ($parts as $part) {
    if (strpos($part, "http") === 0) {
      if (filter_var($part, FILTER_VALIDATE_URL) !== false) {
        return $part;
      }
    }
  }
  return null;
}



?>