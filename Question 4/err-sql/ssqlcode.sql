-- Create Users table
CREATE TABLE Users (
  user_id INT PRIMARY KEY AUTO_INCREMENT,
  username VARCHAR(255) NOT NULL,
  user_type ENUM('engineer', 'project_manager') NOT NULL
);

-- Create Projects table
CREATE TABLE Projects (
  project_id INT PRIMARY KEY AUTO_INCREMENT,
  project_name VARCHAR(255) NOT NULL,
  project_status ENUM('awaiting-start', 'in-progress', 'on-hold') NOT NULL DEFAULT 'awaiting-start',
  created_by INT NOT NULL,
  project_manager INT NOT NULL,
  created_at DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  FOREIGN KEY (created_by) REFERENCES Users(user_id),
  FOREIGN KEY (project_manager) REFERENCES Users(user_id)
);

-- Create Milestones table
CREATE TABLE Milestones (
  milestone_id INT PRIMARY KEY AUTO_INCREMENT,
  project_id INT NOT NULL,
  milestone_name VARCHAR(255) NOT NULL,
  completed BOOLEAN NOT NULL DEFAULT FALSE,
  FOREIGN KEY (project_id) REFERENCES Projects(project_id)
);
