<?php

//function to emove duplicates.
function remove_duplicates($filename) {
    if (!file_exists($filename)) {
      return "Error: File does not exist.";
    }
    $text = file_get_contents($filename);
    $words = explode(" ", strtolower($text));  // Convert to lowercase for case-insensitive matching
    $unique_words = array_unique($words);
    return $unique_words;
  }
  
 
  
// to extract a punction mark.
function extract_punctuation($filename) {
    if (!file_exists($filename)) {
      return "Error: File does not exist.";
    }
    $text = file_get_contents($filename);
    $punctuation = array();
    for ($i = 0; $i < strlen($text); $i++) {
      $char = $text[$i];
      if (in_array($char, array('.', ',', '!', '?', ';', ':', '’', '"', '(', ')'))) {
        $punctuation[] = $char;
      }
    }
    return $punctuation;
  }
  
  
  $punctuation_marks = extract_punctuation('test-file.txt');
  print_r($punctuation_marks);

  //print arary of words and their positions.
  $unique_words = remove_duplicates('test-file.txt');
  print_r($unique_words);
  
?>