<?php

namespace Kanzu\Controllers;

use Kanzu\Models\Milestone;
use Kanzu\Models\Project;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class ProjectController
{
    public function createProject(Request $request, Response $response): Response
    {
        $data = $request->getParsedBody();    
        // Validate project data (name, etc.)
    
        $project = Project::create([
            'name' => $data['name'],
            'developer_id' => $request->getAttribute('userId'),
            'project_manager_id' => isset($data['project_manager_id']) ? $data['project_manager_id'] : null,
        ]);
    
        $milestones = [];
        foreach ($data['milestones'] as $milestoneData) {
            // Validate milestone data (description, etc.)
    
            $milestone = new Milestone([
                'description' => $milestoneData['description'],
                'project_id' => $project->id,
            ]);
            $milestone->save();
            $milestones[] = $milestone;
        }
    
              return $response->withJson([
            'project' => $project,
            'milestones' => $milestones,
        ], 201); // Created
    }
    
}