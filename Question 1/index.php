<?php
function solve($numbers) {
  // Sort the input array
  sort($numbers);

  // Calculate Fibonacci numbers starting from the seventh element
  $fibonacci = [];
  $a = 0;
  $b = 1;
  for ($i = 7; $i < count($numbers); $i++) {
    $fibonacci[] = $b;
    $c = $a + $b;
    $a = $b;
    $b = $c;
  }

  // Return the Fibonacci numbers sorted in descending order
  return array_reverse($fibonacci);
}


//printing values to screen.
$numbers = range(0, 100);
$result = solve($numbers);

// Accessing individual values
for ($i = 0; $i < count($result); $i++) {
  echo $result[$i] . " "; // Print each Fibonacci number
}

?>
