<?php

namespace Kanzu\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
        'name',
        'status',
        'developer_id',
        'project_manager_id',
    ];

    public function milestones()
    {
        return $this->hasMany(Milestone::class);
    }

    public function developer()
    {
        return $this->belongsTo(User::class, 'developer_id');
    }

    public function projectManager()
    {
        return $this->belongsTo(User::class, 'project_manager_id');
    }

    public function canChangeStatusTo($newStatus)
    {
        if ($this->status === 'completed') {
            return false;
        }

        return in_array($newStatus, ['awaiting-start', 'in-progress', 'on-hold']);
    }

    public function isCompleted()
    {
        return $this->status === 'completed';
    }
}
