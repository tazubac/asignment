<?php
declare(strict_types=1);

use App\Application\Actions\User\ListUsersAction;
use App\Application\Actions\User\ViewUserAction;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;

return function (App $app) {
    $app->options('/{routes:.*}', function (Request $request, Response $response) {
        // CORS Pre-Flight OPTIONS Request Handler
        return $response;
    });

    $app->get('/', function (Request $request, Response $response) {
        $response->getBody()->write('Hello world!');
        return $response;
    });

    $app->group('/users', function (Group $group) {
        $group->get('', ListUsersAction::class);
        $group->get('/{id}', ViewUserAction::class);
    });

    //My API END POINTS for Createing Project and Add Milestones (POST /projects):
    $app->post('/projects', function (Request $request, Response $response) use ($db) {
        if (!isLoggedIn($request)) {
          return $response->withStatus(401); // Unauthorized
        }
      
        $data = $request->getParsedBody();
      
        try {
          $db->beginTransaction();
      
          // Insert project data
          $sql = "INSERT INTO Projects (project_name, project_status, created_by, project_manager) VALUES (?, ?, ?, ?)";
          $stmt = $db->prepare($sql);
          $stmt->execute([
            $data['project_name'],
            'awaiting-start', // Default status
            $request->getAttribute('user_id'), // Get from authentication
            $data['project_manager_id'] // Validate ID existence
          ]);
      
          $project_id = $db->lastInsertId();
      
          // Insert milestones
          if (isset($data['milestones']) && is_array($data['milestones'])) {
            foreach ($data['milestones'] as $milestone) {
              $sql = "INSERT INTO Milestones (project_id, milestone_name) VALUES (?, ?)";
              $stmt = $db->prepare($sql);
              $stmt->execute([$project_id, $milestone]);
            }
          }
      
          $db->commit();
      
          $response->getBody()->write(json_encode(['message' => 'Project created successfully']));
        } catch (PDOException $e) {
          $db->rollBack();
          $response->withStatus(500); // Internal Server Error
          $response->getBody()->write(json_encode(['error' => $e->getMessage()]));
        }
      
        return $response;
      });

      //Changing project status
      $app->put('/projects/{id}', function (Request $request, Response $response, $args) use ($db) {
        if (!isLoggedIn($request)) {
            return $response->withStatus(401); // Unauthorized
        }
    
        $project = Project::find($args['id']);
    
        if (!$project) {
            return $response->withStatus(404); // Not Found
        }
    
        // Check if user is an engineer and project developer
        if ($request->getAttribute('userRole') !== 'engineer' || $project->developer_id !== $request->getAttribute('userId')) {
            return $response->withStatus(403); // Forbidden (not authorized to modify)
        }
    
        $data = $request->getParsedBody();
    
        if (isset($data['status']) && $project->canChangeStatusTo($data['status'])) {
            $project->update(['status' => $data['status']]);
        } else {
            return $response->withJson(['error' => 'Invalid status or cannot modify completed projects'], 400); // Bad Request
        }
    
        return $response->withJson($project, 200); // OK
    });
    

    //Returns a list of projects assigned to an engineer
    $app->get('/engineers/{userId}/projects', function (Request $request, Response $response, $args) use ($db) {
    if (!isLoggedIn($request)) {
        return $response->withStatus(401); // Unauthorized
    }

    $user = User::find($args['userId']);

    if (!$user) {
        return $response->withStatus(404); // Not Found (user not found)
    }

    if (!$user->isEngineer()) {
        return $response->withStatus(403); // Forbidden (not authorized to access engineer projects)
    }

    $projects = Project::where('developer_id', $user->id)->get();

    return $response->withJson($projects, 200); // OK
});


//D. Can change the engineer assigned to the project
$app->put('/projects/{id}', function (Request $request, Response $response, $args) use ($db) {
    if (!isLoggedIn($request)) {
        return $response->withStatus(401); // Unauthorized
    }

    $project = Project::find($args['id']);

    if (!$project) {
        return $response->withStatus(404); // Not Found
    }

    // Check user role and authorization (update based on your needs)
    $currentUser = User::find($request->getAttribute('userId'));

    if (!$currentUser) {
        return $response->withStatus(401); // Unauthorized (user not found)
    }

    if (!($currentUser->isProjectManager() || $project->developer_id === $currentUser->id)) {
        return $response->withStatus(403); // Forbidden (not authorized to modify)
    }

    $data = $request->getParsedBody();

    if (isset($data['developer_id'])) {
        $newDeveloper = User::find($data['developer_id']);
        if (!$newDeveloper) {
            return $response->withJson(['error' => 'Invalid developer ID'], 400); // Bad Request
        }

        $project->developer_id = $newDeveloper->id;
        $project->save();
    } else {
        return $response->withJson(['error' => 'Missing developer_id in request body'], 400); // Bad Request
    }

    return $response->withJson($project, 200); // OK
});


//Allows the PM to mark a project as completed.
$app->put('/projects/{id}', function (Request $request, Response $response, $args) use ($db) {
    if (!isLoggedIn($request)) {
        return $response->withStatus(401); // Unauthorized
    }

    $project = Project::find($args['id']);

    if (!$project) {
        return $response->withStatus(404); // Not Found
    }

    $currentUser = User::find($request->getAttribute('userId'));

    if (!$currentUser) {
        return $response->withStatus(401); // Unauthorized (user not found)
    }

    $data = $request->getParsedBody();

    // Check user role and authorization
    if ($currentUser->isEngineer() && isset($data['status']) && $data['status'] === 'completed') {
        return $response->withJson(['error' => 'Engineers cannot mark projects as completed'], 403); // Forbidden
    } else if (!($currentUser->isProjectManager() || $project->developer_id === $currentUser->id)) {
        return $response->withStatus(403); // Forbidden (not authorized to modify)
    }

    if (isset($data['status']) && $project->canChangeStatusTo($data['status'])) {
        $project->update(['status' => $data['status']]);
    } else {
        return $response->withJson(['error' => 'Invalid status or cannot modify completed projects'], 400); // Bad Request
    }

    return $response->withJson($project, 200); // OK
});



};
